#!/usr/bin/python

import os
import xmlrpclib

# pylint: disable=invalid-name

with open('/home/buildd/tokens/staging.token') as auth:
    staging_token = "%s" % auth.read().split('\n')[0]

with open('/home/buildd/tokens/production.token') as auth:
    production_token = "%s" % auth.read().split('\n')[0]

# preserve comments
with open("/home/buildd/refactoring/functional/lxc-dispatcher-stretch.yaml") as f:
    data = f.read()
server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
server.scheduler.submit_job(data)
server = xmlrpclib.ServerProxy("http://lava-auto:%s@validation.linaro.org/RPC2/" % production_token)
server.scheduler.submit_job(data)

# release testing
basedir = '/home/buildd/refactoring/release'
for job_file in os.listdir(basedir):
    if not job_file.endswith('.yaml'):
        continue
    with open(os.path.join(basedir, job_file), 'r') as job_data:
        data = job_data.read()
    server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
    try:
        server.scheduler.submit_job(data)
    except xmlrpclib.Fault:
        pass

# standard test jobs
basedir = '/home/buildd/refactoring/standard'
for job_file in os.listdir(basedir):
    if not job_file.endswith('.yaml'):
        continue
    with open(os.path.join(basedir, job_file), 'r') as job_data:
        data = job_data.read()
    server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
    try:
        server.scheduler.submit_job(data)
    except xmlrpclib.Fault:
        pass

blacklist = [
    'mustang-admin-example-job.yaml',
    'bbb-2serial.yaml',
    'standard-nbd-netboot-bbb.yaml',
    'multiple-serial-ports-lxc.yaml',
    'hikey-new-connection.yaml',
    'examples/test-jobs/hi6220-hikey.yaml',
    'examples/test-jobs/hikey-new-connection.yaml',
    'examples/test-jobs/hikey-oe.yaml',
    'examples/test-jobs/multiple-serial-ports-lxc.yaml',
    'examples/test-jobs/artifact-conversion-download.yaml',
]

# documentation examples
basedir = '/home/buildd/git/lava/doc/v2/examples/test-jobs/'
for job_file in os.listdir(basedir):
    if not job_file.endswith('.yaml'):
        continue
    if [True in set([item.endswith(job_file) for item in blacklist])][0]:
        continue
    with open(os.path.join(basedir, job_file), 'r') as job_data:
        data = job_data.read()
    server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
    try:
        server.scheduler.submit_job(data)
    except xmlrpclib.Fault:
        pass

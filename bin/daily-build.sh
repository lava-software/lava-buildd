#!/bin/sh

set -e

# download from gitlab, quietly.
/home/buildd/repo-update.sh >/dev/null 2>&1

# make above packages available in daily-repo
/home/buildd/bin/repo.sh

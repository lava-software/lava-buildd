#!/bin/sh

set -e

SERVERURL="https://git.linaro.org/lava/lava-server.git/commit/"
DISPATCHERURL="https://git.linaro.org/lava/lava-dispatcher.git/commit/"
cd /home/buildd/v1-archive
HOST="mirror"
DIR="/mirror/images/v1-archive-repo/"
# n.b. no slash on the local dir, must have trailing slash on remote dir
rm -f trace/*
rsync -az --delete production-repo.key.asc ${HOST}:${DIR}
rsync -az --delete dists ${HOST}:${DIR}
rsync -az --delete pool ${HOST}:${DIR}
# rsync -az --delete trace/* ${HOST}:${DIR}

#!/bin/sh

set -e

SERVERURL="https://git.linaro.org/lava/lava-server.git/commit/"
DISPATCHERURL="https://git.linaro.org/lava/lava-dispatcher.git/commit/"
cd /home/buildd/production
HOST="mirror"
DIR="/mirror/images/production-repo/"
# n.b. no slash on the local dir, must have trailing slash on remote dir
NAME=`hostname`-trace.txt
TRACEFILE="trace/${NAME}"
rm -f trace/*
date > ${TRACEFILE}
echo "lava-server" >> ${TRACEFILE}
cat /home/buildd/git/latest-lava-server-release >> ${TRACEFILE}
HASH=`sed -e 's/^commit //' /home/buildd/git/latest-lava-server-release`
echo "${SERVERURL}${HASH}" >> ${TRACEFILE}
echo "lava-dispatcher" >> ${TRACEFILE}
cat /home/buildd/git/latest-lava-dispatcher-release >> ${TRACEFILE}
HASH=`sed -e 's/^commit //' /home/buildd/git/latest-lava-dispatcher-release`
echo "${DISPATCHERURL}${HASH}" >> ${TRACEFILE}
gpg -a --clearsign ${TRACEFILE}
rsync -az --delete production-repo.key.asc ${HOST}:${DIR}
rsync -az --delete dists ${HOST}:${DIR}
rsync -az --delete pool ${HOST}:${DIR}
rsync -az --delete trace/* ${HOST}:${DIR}


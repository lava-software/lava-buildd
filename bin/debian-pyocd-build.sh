#!/bin/sh

set -e

# Requirements: sbuild (jessie schroot), git, reprepro.
# customised for pyOCD due to additional steps.

BUILDD_BIN=/home/buildd/bin/
GIT_TREE_DIR=/home/buildd/git/
GIT_BRANCH=master
LATEST_SUFFIX='-master'
CHANGELOG_ENTRY="New build for Linaro."
REPO_DIR=/home/buildd/debian
GIT_DEBIAN_BRANCH=master
BUILD_SUITE=jessie
SCHROOT=jessie
SUITE=jessie
BACKPORTS_SUITE=jessie-backports

if [ -n "$2" ]; then
    set +e
    chk=`dpkg-architecture -a$2 > /dev/null 2>&1 ; echo $?`
    set -e
    if [ $chk = 0 ]; then
        echo "Building for architecture $2"
        arch="$2"
    else
        echo "Did not recognise $2 as a Debian architecture name. Exit."
        exit 1
    fi
fi
host_arch=`dpkg-architecture -qDEB_HOST_ARCH`
PWD=`pwd`
NAME='pyOCD'
PKG_NAME='pyocd'
suite=${SUITE}
schroot=${SCHROOT}
backports=${BACKPORTS_SUITE}
cd ${GIT_TREE_DIR}${NAME}
git checkout ${GIT_BRANCH}
git pull
if [ -f ../latest-${NAME}${LATEST_SUFFIX} ]; then
    LATEST=`cat ../latest-${NAME}${LATEST_SUFFIX}`
    NOW=`git log -n1 |grep commit`
    if [ "$LATEST" = "$NOW" ]; then
        echo Latest git commit built was: ${LATEST}
        exit 0
    fi
fi
VERSION=`python setup.py --version`
if [ -d './dist/' ]; then
    rm -f ./dist/*
fi
python setup.py sdist 2>&1
if [ -d .git ]; then
  LOG=`git log -n1 --pretty=format:"Last change %h by %an, %ar. %s%n" --no-merges`
fi
# run the convert utility.
../pkg-pyocd/debian/convert.py
echo `pwd`
DIR=`mktemp -d`
mv -v ./dist/${PKG_NAME}-${VERSION}.tar.gz ${DIR}/${PKG_NAME}_${VERSION}.orig.tar.gz
cd ${DIR}
tar -xaf ${PKG_NAME}_${VERSION}.orig.tar.gz
cd ${GIT_TREE_DIR}pkg-${PKG_NAME}
git checkout master
git pull
git archive ${GIT_DEBIAN_BRANCH} debian | tar -x -C ${DIR}/${PKG_NAME}-${VERSION}
cd ${DIR}/${PKG_NAME}-${VERSION}
dch --force-distribution -v ${VERSION}-1 -D ${BACKPORTS_SUITE} ${CHANGELOG_ENTRY} 2>&1
if [ -n "${LOG}" ]; then
    dch -a ${LOG}
fi
# debuild -A -sa -uc -us
sbuild --build-dep-resolver=aptitude --force-orig-source -A -s -c ${schroot} -d ${suite}
reprepro --ignore=wrongdistribution -b ${REPO_DIR} include ${BACKPORTS_SUITE} ${DIR}/${PKG_NAME}_${VERSION}-1_${host_arch}.changes
if [ -n "$arch" ]; then
    sbuild --arch ${arch} -c ${schroot}-${arch} -d ${suite}
    reprepro --ignore=wrongdistribution -b ${REPO_DIR} include ${BACKPORTS_SUITE} ${DIR}/${PKG_NAME}_${VERSION}-1_${arch}.changes
fi
cd ${DIR}
rm -rf ${DIR}/pkg-${PKG_NAME}
rm -rf ${DIR}/${PKG_NAME}-${VERSION}
cd ${GIT_TREE_DIR}${NAME}
git checkout ${GIT_BRANCH}
git log -n1 |grep commit > ${GIT_TREE_DIR}latest-${NAME}${LATEST_SUFFIX}
git checkout master
rm -rf ${DIR}/
reprepro -b ${REPO_DIR} ls ${PKG_NAME}


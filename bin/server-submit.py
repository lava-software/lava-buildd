#!/usr/bin/python

import xmlrpclib

# pylint: disable=invalid-name

with open('/home/buildd/tokens/staging.token') as auth:
    staging_token = "%s" % auth.read().split('\n')[0]

with open('/home/buildd/tokens/production.token') as auth:
    production_token = "%s" % auth.read().split('\n')[0]

with open("/home/buildd/refactoring/functional/lxc-server-stretch.yaml") as f:
    config = f.read()

server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
job_id = server.scheduler.submit_job(config)
server = xmlrpclib.ServerProxy("http://lava-auto:%s@validation.linaro.org/RPC2/" % production_token)
job_id = server.scheduler.submit_job(config)

# install tests
install = "/home/buildd/refactoring/functional/server-staging-install-stretch.yaml"
qemu_di = "/home/buildd/refactoring/functional/qemu-jessie-installer.yaml"

#with open(upgrade) as f:
#    config = f.read()
server = xmlrpclib.ServerProxy("http://lavabuildd:%s@staging.validation.linaro.org/RPC2/" % staging_token)
#job_id = server.scheduler.submit_job(config)

with open(install) as f:
    config = f.read()
job_id = server.scheduler.submit_job(config)

with open(qemu_di) as f:
    config = f.read()
job_id = server.scheduler.submit_job(config)

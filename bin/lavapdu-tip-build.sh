#!/bin/sh

#### This script is specific to Lavapdu as we want the same version in staging and production

set -e

if [ -n "$1" ]; then
	echo "This script only builds Lavapdu.."
	exit 1
fi

PWD=`pwd`
NAME="lavapdu"
suite=unstable
schroot=sid-amd64-sbuild
cd /home/buildd/git/${NAME}
git checkout master
git pull
if [ -f ../latest-${NAME} ]; then
	LATEST=`cat ../latest-${NAME}`
	NOW=`git log -n1 |grep commit`
	if [ "$LATEST" = "$NOW" ]; then
		echo Latest git commit built was: ${LATEST}
		exit 0
	fi
fi
VERSION=`cat setup.py  | grep version= | sed 's/.*version=\"//' | sed 's/\".*//'`
#VERSION=`python ./version.py`
if [ -d './dist/' ]; then
	rm -f ./dist/*
fi
python setup.py sdist 2>&1
DIR=`mktemp -d`
mkdir ${DIR}/${NAME}-${VERSION}
mv -v ./dist/${NAME}-${VERSION}.tar.gz ${DIR}/${NAME}_${VERSION}.orig.tar.gz
tar -xzf ${DIR}/${NAME}_${VERSION}.orig.tar.gz -C ${DIR}
cd ../pkg-${NAME}
git pull
dpkg-checkbuilddeps
git archive master debian | tar -x -C ${DIR}/${NAME}-${VERSION}
cd ${DIR}/${NAME}-${VERSION}
dch -b --force-distribution -v ${VERSION}-1 -D unstable "Lavapdu incremental build" 2>&1
#debuild -sa -uc -us $arch
sbuild -A -s -c ${schroot} -d ${suite} --force-orig-source
cd ${DIR}
rm -rf ${DIR}/pkg-${NAME}
rm -rf ${DIR}/${NAME}-${VERSION}
reprepro -b /home/buildd/debian include sid ${DIR}/${NAME}_${VERSION}*.changes
reprepro -b /home/buildd/production include sid ${DIR}/${NAME}_${VERSION}*.changes
cd /home/buildd/git/${NAME}
git log -n1 |grep commit > /home/buildd/git/latest-${NAME}
rm -rf ${DIR}/
reprepro -b /home/buildd/debian ls ${NAME}
reprepro -b /home/buildd/production ls ${NAME}


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  gitlab-api.py
#
#  Copyright 2018 Neil Williams <neil.williams@linaro.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import sys
import yaml
import argparse
import requests
import zipfile


BASE = "https://git.lavasoftware.org/api/v4/projects/2"


def get_data(response):
    if not response:
        raise RuntimeError("Unexpected response")
    if response.status_code != 200:
        raise RuntimeError("Unexpected response")

    data = yaml.safe_load(response.text)
    if not isinstance(data, list):
        raise RuntimeError("Unexpected response")
    return data

def decompress(archive_data, output):
    artifacts = os.path.join(os.getcwd(), "artifacts", "%s.zip" % archive_data["id"])
    content = os.path.join(os.getcwd(), "artifacts", "build")
    if os.path.exists(artifacts):
        msg = "Not overwriting %s" % artifacts
        raise RuntimeError(msg)
    print("Starting download of artifacts for job %s" % archive_data["id"])
    response = requests.get(BASE + "/jobs/%s/artifacts" % archive_data["id"], stream=True)
    if response.status_code != 200:
        raise RuntimeError("Unable to download artifacts")
    handle = open(artifacts, "wb")

    for chunk in response.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)
    handle.close()

    with zipfile.ZipFile(artifacts) as zipdata:
        for pkg in zipdata.namelist():
            if not pkg.startswith("build/"):
                raise RuntimeError(
                    "Unexpected content - %s might be unsafe" % artifacts
                )
            zipdata.extract(pkg, path=output)
    # consumer of the artifacts output should remove
    # the build files and directory.
    os.unlink(artifacts)


def main():
    parser = argparse.ArgumentParser(description="Download packages from GitLab")
    parser.add_argument(
        "-t", "--tag", help="Name of the tag, if any."
    )
    args = parser.parse_args()
    ref_name = "master"
    if args.tag:
        print("Downloading artifacts for tag %s" % args.tag)
        ref_name = args.tag
    with open("./tokens/lso.token") as auth:
        lso_token = "%s" % auth.read().split("\n")[0]

    headers = {"PRIVATE-TOKEN": lso_token}

    params = (
        ("status", "success"),
        ("ref", ref_name),
        ("order_by", "id"),
        ("sort", "desc"),
    )
    latest = 0
    output = os.path.join(os.getcwd(), "artifacts")
    if not os.path.exists(output):
        os.mkdir(output)
    latest_filename = os.path.join(output, "latest")
    if os.path.exists(latest_filename):
        with open(latest_filename, "r") as latest_file:
            latest = latest_file.read()
    response = requests.get(BASE + "/pipelines", headers=headers, params=params)
    data = get_data(response)
    if not data:
        raise RuntimeError("No results for successful master pipeline.")
    if latest == "%s" % data[0]["id"]:
        msg = "Latest build %s has already been processed." % latest
        raise RuntimeError(msg)
    print("Downloading artifacts of pipeline ID %s" % data[0]["id"])
    latest = data[0]["id"]
    response = requests.get(
        BASE + "/pipelines/%s/jobs" % data[0]["id"], headers=headers
    )
    data = get_data(response)
    content = os.path.join(os.getcwd(), "artifacts", "build")
    if os.path.exists(content):
        msg = "Content directory %s already exists" % content
        raise RuntimeError(msg)
    # stretch amd64
    stretch_debian_pkg = [job for job in data if job["name"] == "amd64/pkg-debian-9"][0]
    decompress(stretch_debian_pkg, output)
    # stretch aarch64
    stretch_arm64 = [job for job in data if job["name"] == "aarch64/pkg-debian-9"]
    if stretch_arm64:
	    decompress(stretch_arm64[0], output)
    # buster amd64
    buster_amd64 = [job for job in data if job["name"] == "amd64/pkg-debian-10"][0]
    decompress(buster_amd64, output)
    # buster arm64
    buster_arm64 = [job for job in data if job["name"] == "aarch64/pkg-debian-10"][0]
    decompress(buster_arm64, output)
    with open(latest_filename, "w") as latest_file:
        latest_file.write("%d\n" % latest)
    return 0


if __name__ == "__main__":
    sys.exit(main())

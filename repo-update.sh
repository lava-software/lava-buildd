#!/bin/sh

set -e

TAG=""
DRY=0
BASE="/home/buildd/"
DAILY="debian"
RELEASE="release"
R_OPT="--ignore=wrongdistribution"

while getopts ":t:n" opt; do
  case $opt in
    t)
        TAG=${OPTARG}
        DAILY=${RELEASE}
        ;;
    n)
        DRY=1
        ;;
    \?)
      echo "[LAVA-REPO] Invalid option: -$OPTARG" >&2
      echo
      echo "Usage: [-t <tag_name> -s <server> -n]"
      echo
      echo "Specify the tag name to update the production"
      echo "or release repositories with that tag"
      echo "Use -n to skip the download from GitLab"
      echo "if the files already exist"
      exit 1
      ;;
  esac
done

if [ ${DRY} = 0 ]; then
  rm -rf ./artifacts/build/
  # needs to pass the tag name for a release
  if [ -n "${TAG}" ]; then
    ./gitlab-api.py -t ${TAG}
  else
    ./gitlab-api.py
  fi
fi


BASEDIR="${BASE}/${DAILY}"

if [ -d "${BASEDIR}/dists/stretch-backports" ]; then
    reprepro -b ${BASEDIR} ${R_OPT} include stretch-backports /home/buildd/lava-buildd/artifacts/build/lava_*stretch_amd64.changes
    reprepro -b ${BASEDIR} list stretch-backports
fi
if [ -d "${BASEDIR}/dists/buster" ]; then
    reprepro -b ${BASEDIR} include buster ${BASE}/lava-buildd/artifacts/build/lava_*buster_amd64.changes
    reprepro -b ${BASEDIR} include buster ${BASE}/lava-buildd/artifacts/build/lava_*buster_arm64.changes
    reprepro -b ${BASEDIR} list buster
fi

# location of snapshot directory, if any.
SNAPSHOT=/var/www/apt.lavasoftware.org/snapshot/

if [ ! -d ${SNAPSHOT} ]; then
   # images.validation.linaro.org
   exit
fi

YEAR=`date +%Y`
MONTH=`date +%m`
DAY=`date +%d`

if [ -d "${BASEDIR}/dists/stretch-backports" ]; then
    mkdir -p ${SNAPSHOT}/stretch/${YEAR}/${MONTH}/${DAY}/
    dcmd cp ${BASE}/lava-buildd/artifacts/build/lava_*stretch_*.changes ${SNAPSHOT}/stretch/${YEAR}/${MONTH}/${DAY}/
fi
if [ -d "${BASEDIR}/dists/buster" ]; then
    mkdir -p ${SNAPSHOT}/buster/${YEAR}/${MONTH}/${DAY}/
    dcmd cp ${BASE}/lava-buildd/artifacts/build/lava_*buster_*.changes ${SNAPSHOT}/buster/${YEAR}/${MONTH}/${DAY}/
fi
